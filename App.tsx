import { useState } from 'react'
import './App.css'

function App() {

  return (
    <>
      <div class="container">
        <form>
          <h1 >Entrar na plataforma</h1>
          <div class="input-container">
            <input  placeholder="Email" type="email" required/>
            <img width="30" height="30" src="https://img.icons8.com/material-two-tone/48/user.png" alt="user"/>
          </div>
          <div class="input-container">
            <input  placeholder="password" type="password"  required/>
            <img width="30" height="30" src="https://img.icons8.com/ios-glyphs/30/lock-2.png" alt="lock-2"/>
            <a href="#">Esqueci minha senha</a>
          </div>
          <button class="submit-button" type="submit">Entrar</button>
          <div class="register-link">
            <p>Não está cadastrado ? <a href="#">Cadastrar</a></p>
          </div>
        </form>
      </div>
    </>
  )
}

export default App
